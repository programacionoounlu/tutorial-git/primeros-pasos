// Clase que implementa la funcionalidad de una calculadora
/*
 * 1. La calculadora debe permitir ejecutar sumas
 * 2. La calculadora debe permitir ejecutar restas
 */
public class Calculadora {

    public double suma(int a, int b) {
        return a+b;
    }

    public void resta(int a, int b) {
        return a-b;
    }
}
